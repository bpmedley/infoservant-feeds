#!/bin/bash

cd docroot

/opt/ActivePerl-5.18/bin/perl -MPOSIX -p -i -e '$t=POSIX::strftime("%Y-%m-%d", localtime); if (/=== START/ .. /=== STOP/ and !/START|STOP/) { $r=qr/"(\d+-\d+-\d+)\.(\d+)"/; m/$r/; $d=$1; $n=$2; $day{$d} = $n; $day{$t}++; $day{$t} = sprintf("%03d", $day{$t}); s/$r/"$t.$day{$t}"/; }' lib/InfoServant.pm

\rm -f infoservant_feeds.tgz
tar -czf ../infoservant_feeds.tgz includes

/opt/pdk/bin/perlapp \
	--add "Mojo::;Mojolicious::;InfoServant::" \
	--bind "infoservant_feeds.tgz[file=../infoservant_feeds.tgz]" \
	--norunlib  \
	--clean  \
	--lib lib \
	--target linux-x86-64 \
	--force \
	--exe ../app/infoservant_feeds-linux-x64  \
	--perl /opt/ActivePerl-5.18/bin/perl-static  \
	script/infoservant_feeds

exit

/usr/local/PDK/bin/perlapp \
	--add "Mojo::;Mojolicious::;Sparky::;B::Hooks::EndOfScope::" \
    --add "Sparky::Index" \
    --add "Sparky::Dashboard" \
    --add "Sparky::Public" \
	--bind "entities.txt[file=lib/Mojo/entities.txt,extract]" \
	--bind "sparky.tgz[file=sparky.tgz]" \
	--lib lib \
	--lib lib/Sparky \
	--norunlib \
	--force \
	--exe app/sparky-osx \
	script/sparky

/usr/local/PDK/bin/perlapp \
	--add "Mojo::;Mojolicious::;Sparky::;B::Hooks::EndOfScope::" \
    --add "Sparky::Index" \
    --add "Sparky::Dashboard" \
    --add "Sparky::Public" \
	--bind "entities.txt[file=lib/Mojo/entities.txt,extract]" \
	--bind "sparky.tgz[file=sparky.tgz]" \
	--lib lib \
	--lib lib/Sparky \
	--norunlib \
	--target windows-x86-32 \
	--force \
	--exe app/sparky-win32.exe \
	script/sparky

/usr/local/PDK/bin/perlapp \
	--add "Mojo::;Mojolicious::;Sparky::;B::Hooks::EndOfScope::" \
    --add "Sparky::Index" \
    --add "Sparky::Dashboard" \
    --add "Sparky::Public" \
	--bind "entities.txt[file=lib/Mojo/entities.txt,extract]" \
	--bind "sparky.tgz[file=sparky.tgz]" \
	--lib lib \
	--lib lib/Sparky \
	--norunlib \
	--target linux-x86-32 \
	--force \
	--exe app/sparky-linux-x86-32 \
	script/sparky

/usr/local/PDK/bin/perlapp \
	--add "Mojo::;Mojolicious::;Sparky::;B::Hooks::EndOfScope::" \
    --add "Sparky::Index" \
    --add "Sparky::Dashboard" \
    --add "Sparky::Public" \
	--bind "entities.txt[file=lib/Mojo/entities.txt,extract]" \
	--bind "sparky.tgz[file=sparky.tgz]" \
	--lib lib \
	--lib lib/Sparky \
	--norunlib \
	--target linux-x86-64 \
	--force \
	--exe app/sparky-linux-x86-64 \
	script/sparky

